"""
    Fichier : gestion_personne_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les employés.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from EMP_PERSONNE import obj_mon_application
from EMP_PERSONNE.database.connect_db_context_manager import MaBaseDeDonnee
from EMP_PERSONNE.erreurs.msg_erreurs import *
from EMP_PERSONNE.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /employer_delete
    
    Test : ex. cliquer sur le menu "employés" puis cliquer sur le bouton "DELETE" d'un "employer"
    
    Paramètres : sans
    
    But : Effacer(delete) un employer qui a été sélectionné dans le formulaire "employé_afficher.html"
    
    Remarque :  Dans le champ "nom_employer_delete_wtf" du formulaire "employés/personne_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    employer_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_employer"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_employer.data:

            if form_demo.submit_btn_ok_dplist_employer.data:
                print("employer sélectionné : ",
                      form_demo.employés_dropdown_wtf.data)
                employer_selectionne = form_demo.employés_dropdown_wtf.data
                form_demo.employés_dropdown_wtf.choices = session['employer_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_employés_afficher = """SELECT id_employer, Nom_employer, Prenom_employer FROM t_employer 
                    ORDER BY id_employer ASC """
                mc_afficher.execute(strsql_employés_afficher)

            data_employés = mc_afficher.fetchall()
            print("demo_select_wtf data_employés ", data_employés, " Type : ", type(data_employés))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            employer_val_list_dropdown = []
            for i in data_employés:
                employer_val_list_dropdown.append(i['Nom_employer'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # employer_val_list_dropdown = [(i["id_employer"], i["nom_employer"]) for i in data_employés]

            print("employer_val_list_dropdown ", employer_val_list_dropdown)

            form_demo.employés_dropdown_wtf.choices = employer_val_list_dropdown
            session['employer_val_list_dropdown'] = employer_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.employés_dropdown_wtf.data = "philosophique"
            employer_selectionne = form_demo.employés_dropdown_wtf.data
            print("employer choisi dans la liste :", employer_selectionne)
            session['employer_selectionne_get'] = employer_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           employer_selectionne=employer_selectionne)
