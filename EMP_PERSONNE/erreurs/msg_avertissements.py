"""
    Fichier : msg_avertissements.py
    Auteur : OM 2021.05.02

    Messages d'avertissement. Souvent à caractère informatif.
    Certains peuvent sembler être abrupts.

"""
from flask import render_template
from EMP_PERSONNE import obj_mon_application


@obj_mon_application.route("/avertissement_sympa_pour_geeks")
def avertissement_sympa_pour_geeks():
    # Envoie la page "HTML" au serveur.
    return render_template("emprunter_materiel_personne/avertissement_projet.html")