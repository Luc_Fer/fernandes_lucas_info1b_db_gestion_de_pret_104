"""
    Fichier : gestion_personne_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterEmployés(FlaskForm):
    """
        Dans le formulaire "emprunter_materiel_personne_ajouter.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_employer_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_employer_wtf = StringField("Ajouter l'employer suivant :",
                                   validators=[Length(min=2, max=20, message="min 2 max 20"),
                                               Regexp(nom_employer_regexp,
                                                      message="Pas de chiffres, de caractères "
                                                              "spéciaux, "
                                                              "d'espace à double, de double "
                                                              "apostrophe, de double trait union")
                                               ])

    submit = SubmitField("Enregistrer employer")


class FormWTFUpdateEmployer(FlaskForm):
    """
        Dans le formulaire "personne_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_employer_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_employer_update_wtf = StringField("Éditer l'employer suivant :",
                                          validators=[Length(min=5, max=50, message="min 5 max 50"),
                                                      Regexp(nom_employer_update_regexp,
                                                             message="Pas de chiffres, de "
                                                                     "caractères "
                                                                     "spéciaux, "
                                                                     "d'espace à double, de double "
                                                                     "apostrophe, de double trait "
                                                                     "union")
                                                      ])

    submit = SubmitField("Update employer")


class FormWTFDeleteEmployer(FlaskForm):
    """
        Dans le formulaire "personne_delete_wtf.html"

        nom_employer_delete_wtf : Champ qui reçoit la valeur du employer, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "employer".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_employer".
    """
    nom_employer_delete_wtf = StringField("Effacer l'employer suivant :")
    submit_btn_del = SubmitField("Effacer employer")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")

