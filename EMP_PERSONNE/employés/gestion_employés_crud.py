"""
    Fichier : gestion_personne_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les employer.
"""

import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from EMP_PERSONNE import obj_mon_application
from EMP_PERSONNE.database.connect_db_context_manager import MaBaseDeDonnee
from EMP_PERSONNE.erreurs.exceptions import *
from EMP_PERSONNE.erreurs.msg_erreurs import *
from EMP_PERSONNE.employés.gestion_employés_wtf_forms import FormWTFAjouterEmployés
from EMP_PERSONNE.employés.gestion_employés_wtf_forms import FormWTFDeleteEmployer
from EMP_PERSONNE.employés.gestion_employés_wtf_forms import FormWTFUpdateEmployer


"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /employés_afficher
    
    Test : ex : http://127.0.0.1:5005/employés_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_employer_sel = 0 >> tous les employer.
                id_employer_sel = "n" affiche le employer dont l'id est "n"
"""


@obj_mon_application.route("/employés_afficher/<string:order_by>/<int:id_employer_sel>", methods=['GET', 'POST'])
def employés_afficher(order_by, id_employer_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans employer ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionEmployés {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_employer_sel == 0:
                    strsql_employés_afficher = """SELECT id_employer, Nom_employer FROM t_employer 
                    ORDER BY id_employer ASC """
                    mc_afficher.execute(strsql_employés_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_employer"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du employer sélectionné avec un nom de variable
                    valeur_id_employer_selected_dictionnaire = {"value_id_employer_selected": id_employer_sel}
                    strsql_employés_afficher = """SELECT id_employer, Nom_employer FROM t_employer WHERE id_employer = %(value_id_employer_selected)s """

                    mc_afficher.execute(strsql_employés_afficher, valeur_id_employer_selected_dictionnaire)
                else:
                    strsql_employés_afficher = """SELECT id_employer, Nom_employer FROM t_employer 
                    ORDER BY id_employer DESC """

                    mc_afficher.execute(strsql_employés_afficher)

                data_employés = mc_afficher.fetchall()

                print("data_employés ", data_employés, " Type : ", type(data_employés))

                # Différencier les messages si la table est vide.
                if not data_employés and id_employer_sel == 0:
                    flash("""La table "t_employer" est vide. !!""", "warning")
                elif not data_employés and id_employer_sel > 0:
                    # Si l'utilisateur change l'id_employer dans l'URL et que le employer n'existe pas,
                    flash(f"L'employer demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_employer" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données employés affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. employés_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier
            # "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} employés_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("employés/employés_afficher.html", data=data_employés)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /employés_ajouter
    
    Test : ex : http://127.0.0.1:5005/employés_ajouter
    
    Paramètres : sans
    
    But : Ajouter un employer pour un film
    
    Remarque :  Dans le champ "name_employer_html" du formulaire "employés/employés_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/employés_ajouter", methods=['GET', 'POST'])
def employés_ajouter_wtf():
    form = FormWTFAjouterEmployés()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion employés ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionEmployés {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_employer_wtf = form.nom_employer_wtf.data
                name_employer = name_employer_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_nom_employer": name_employer}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_employer = """INSERT INTO t_employer (id_employer, Nom_employer) VALUES 
                (NULL, %(value_nom_employer)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_employer, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('employés_afficher', order_by='DESC', id_employer_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_employer_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_employer_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion employés CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Employés/employés_ajouter_wtf.html", form=form)

"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /employer_update
    
    Test : ex cliquer sur le menu "employés" puis cliquer sur le bouton "EDIT" d'un "employer"
    
    Paramètres : sans
    
    But : Editer(update) un employer qui a été sélectionné dans le formulaire "employés_afficher.html"
    
    Remarque :  Dans le champ "nom_employer_update_wtf" du formulaire "employés/personne_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/employer_update", methods=['GET', 'POST'])
def employer_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_employer"
    id_employer_update = request.values['id_employer_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateEmployer()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "personne_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_employer_update = form_update.nom_employer_update_wtf.data
            name_employer_update = name_employer_update.lower()

            valeur_update_dictionnaire = {"value_id_employer": id_employer_update,
                                          "value_name_employer": name_employer_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleemployer = """UPDATE t_employer SET Nom_employer = %(value_name_employer)s WHERE id_employer = %(value_id_employer)s """
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleemployer, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_employer_update"
            return redirect(url_for('employés_afficher', order_by="ASC", id_employer_sel=id_employer_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_employer" et "Nom_employer" de la "t_employer"
            str_sql_id_employer = "SELECT id_employer, Nom_employer FROM t_employer WHERE id_employer = %(value_id_employer)s "
            valeur_select_dictionnaire = {"value_id_employer": id_employer_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_employer, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom employer" pour l'UPDATE
            data_nom_employer = mybd_curseur.fetchone()
            print("data_nom_employer ", data_nom_employer, " type ", type(data_nom_employer), " employer ",
                  data_nom_employer["Nom_employer"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_update_wtf.html"
            form_update.nom_employer_update_wtf.data = data_nom_employer["Nom_employer"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans employer_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans employer_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans employer_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans employer_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("employés/employer_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /employer_delete
    
    Test : ex. cliquer sur le menu "employés" puis cliquer sur le bouton "DELETE" d'un "employer"
    
    Paramètres : sans
    
    But : Effacer(delete) un employer qui a été sélectionné dans le formulaire "employés_afficher.html"
    
    Remarque :  Dans le champ "nom_employer_delete_wtf" du formulaire "employés/personne_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/employer_delete", methods=['GET', 'POST'])
def employer_delete_wtf():
    data_personnes_attribue_employer_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_employer"
    id_employer_delete = request.values['id_employer_btn_delete_html']

    # Objet formulaire pour effacer le employer sélectionné.
    form_delete = FormWTFDeleteEmployer()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("employés_afficher", order_by="ASC", id_employer_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau le formulaire "employés/personne_delete_wtf.html"
                # lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_personnes_attribue_employer_delete = session['data_personnes_attribue_employer_delete']
                print("data_personnes_attribue_employer_delete ", data_personnes_attribue_employer_delete)

                flash(f"Effacer le employer de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer employer" qui va irrémédiablement EFFACER le employer
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_employer": id_employer_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_emprunter_materiel_personne = """DELETE FROM t_emprunter_materiel_personne WHERE fk_Employer = %(value_id_employer)s """
                str_sql_delete_idemployer = """DELETE FROM t_employer WHERE id_employer = %(value_id_employer)s"""
                # Manière brutale d'effacer d'abord la "fk_Employer", même si elle n'existe pas dans la
                # "t_employer_film" Ensuite on peut effacer le employer vu qu'il n'est plus "lié" (INNODB) dans la
                # "t_employer_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_emprunter_materiel_personne, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idemployer, valeur_delete_dictionnaire)

                flash(f"Employer définitivement effacé !!", "success")
                print(f"Employer définitivement effacé !!")

                # afficher les données
                return redirect(url_for('employés_afficher', order_by="ASC", id_employer_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_employer": id_employer_delete}
            print(id_employer_delete, type(id_employer_delete))

            # Requête qui affiche tous les films qui ont le employer que l'utilisateur veut effacer
            str_sql_emprunter_materiel_personne_delete = """SELECT id_emprunter_materiel_personne, Nom_Personne, id_employer, Nom_employer FROM t_emprunter_materiel_personne 
                                                            INNER JOIN t_personne ON t_emprunter_materiel_personne.fk_Personne = t_personne.id_Personne
                                                            INNER JOIN t_employer ON t_emprunter_materiel_personne.fk_Employer = t_employer.id_employer
                                                            WHERE fk_Employer = %(value_id_employer)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_emprunter_materiel_personne_delete, valeur_select_dictionnaire)
            data_personnes_attribue_employer_delete = mybd_curseur.fetchall()
            print("data_personnes_attribue_employer_delete...", data_personnes_attribue_employer_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau le formulaire
            # "employés/personne_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_personnes_attribue_employer_delete'] = data_personnes_attribue_employer_delete

            # Opération sur la BD pour récupérer "id_employer" et "Nom_employer" de la "t_employer"
            str_sql_id_employer = "SELECT id_employer, Nom_employer FROM t_employer WHERE id_employer = %(value_id_employer)s "

            mybd_curseur.execute(str_sql_id_employer, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom employer" pour l'action DELETE
            data_nom_employer = mybd_curseur.fetchone()
            print("data_nom_employer", data_nom_employer, " type ", type(data_nom_employer), " employer ",
                  data_nom_employer["Nom_employer"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "personne_delete_wtf.html"
            form_delete.nom_employer_delete_wtf.data = data_nom_employer["Nom_employer"]

            # Le bouton pour l'action "DELETE" dans le form. "personne_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans employer_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans employer_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans employer_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans employer_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("employés/employer_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personnes_associes=data_personnes_attribue_employer_delete)
