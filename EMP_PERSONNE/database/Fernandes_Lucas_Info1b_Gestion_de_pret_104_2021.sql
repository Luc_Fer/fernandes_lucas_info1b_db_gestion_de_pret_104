-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 13 Juin 2021 à 21:33
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fernandes_lucas_info1b_gestion_de_pret_104_2021`
--
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS fernandes_lucas_info1b_gestion_de_pret_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS fernandes_lucas_info1b_gestion_de_pret_104_2021;

-- Utilisation de cette base de donnée

USE fernandes_lucas_info1b_gestion_de_pret_104_2021;

-- --------------------------------------------------------

--
-- Structure de la table `t_employer`
--

CREATE TABLE `t_employer` (
  `ID_employer` int(11) NOT NULL,
  `Nom_employer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_employer`
--

INSERT INTO `t_employer` (`ID_employer`, `Nom_employer`) VALUES
(2, 'omari laagar'),
(3, 'alexandre quirighetti'),
(4, 'alvarez brandon'),
(5, 'giovanne ruggero'),
(6, 'tom rey'),
(7, 'fernando costas'),
(21, 'lucas fernandes');

-- --------------------------------------------------------

--
-- Structure de la table `t_emprunter_materiel_personne`
--

CREATE TABLE `t_emprunter_materiel_personne` (
  `ID_emprunter_materiel_personne` int(11) NOT NULL,
  `FK_Employer` int(20) NOT NULL,
  `FK_Personne` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emprunter_materiel_personne`
--

INSERT INTO `t_emprunter_materiel_personne` (`ID_emprunter_materiel_personne`, `FK_Employer`, `FK_Personne`) VALUES
(22, 3, 24),
(23, 4, 24),
(28, 2, 31),
(29, 3, 31),
(30, 5, 31),
(31, 6, 31);

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `ID_Personne` int(11) NOT NULL,
  `Nom_Personne` varchar(20) NOT NULL,
  `Prenom_Personne` varchar(20) NOT NULL,
  `Telephone_Personne` int(12) NOT NULL,
  `Mail_Personne` varchar(50) NOT NULL,
  `Entite` varchar(20) NOT NULL,
  `Besoin_Materiels` varchar(50) NOT NULL,
  `Date_De_Remise_Materiels` varchar(20) NOT NULL,
  `Date_De_Retour_Materiels` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`ID_Personne`, `Nom_Personne`, `Prenom_Personne`, `Telephone_Personne`, `Mail_Personne`, `Entite`, `Besoin_Materiels`, `Date_De_Remise_Materiels`, `Date_De_Retour_Materiels`) VALUES
(24, 'scoflief', 'michael', 212524503, 'akd@gmail.comd', 'akd@gmail.comd', 'fddfdsafcd', '2020-05-06', '2021-05-06'),
(31, 'hidolft', 'titouen', 2125245036, 'hidolft.hokager@gmail.com', 'atraction', 'unlaptopetalimentation', '2021-06-13', '2021-06-21'),
(32, 'pute', 'aiden', 795226421, 'aiden.lemagicienmagic@jaimelamagic.com', 'dfc', 'uneputepaschere', '2021-04-13', '2021-10-11');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_employer`
--
ALTER TABLE `t_employer`
  ADD PRIMARY KEY (`ID_employer`);

--
-- Index pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  ADD PRIMARY KEY (`ID_emprunter_materiel_personne`),
  ADD KEY `FK_Employer` (`FK_Employer`),
  ADD KEY `FK_Personne` (`FK_Personne`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`ID_Personne`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_employer`
--
ALTER TABLE `t_employer`
  MODIFY `ID_employer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  MODIFY `ID_emprunter_materiel_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `ID_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_emprunter_materiel_personne`
--
ALTER TABLE `t_emprunter_materiel_personne`
  ADD CONSTRAINT `t_emprunter_materiel_personne_ibfk_1` FOREIGN KEY (`FK_Personne`) REFERENCES `t_personne` (`ID_Personne`),
  ADD CONSTRAINT `t_emprunter_materiel_personne_ibfk_2` FOREIGN KEY (`FK_Employer`) REFERENCES `t_employer` (`ID_employer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
